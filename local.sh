#!/bin/bash

# docker-compose build
# docker-compose up -d
# composer install
# ./local.sh

cp .env.example .env
chmod -R 777 storage
php artisan cache:clear
php artisan key:generate